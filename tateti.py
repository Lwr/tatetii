import unittest
import i18n

class PositionOutOfBounds(Exception):
    pass

class APlayerCantPlayTwiceInARow(Exception):
	pass


class Tateti:

	possibleValues = [i18n.EMPTY, i18n.X, i18n.O]

	

	def __init__(self):
		self.array = [[i18n.EMPTY, i18n.EMPTY, i18n.EMPTY],[i18n.EMPTY, i18n.EMPTY, i18n.EMPTY],[i18n.EMPTY, i18n.EMPTY, i18n.EMPTY]]
		self.turn = 'Nothing'
	
	

	def isEmpty(self, x, y):
		'''
		@param int x :: x \in [1,3]
		@param int x :: x \in [1,3]
		'''
		return self.array[x-1][y-1] == i18n.EMPTY

	
	def putX(self, x, y):
		self.__handleElem(x,y,i18n.X)

	def putO(self, x, y):
		self.__handleElem(x,y,i18n.O)


	def __handleElem(self, x, y,elem):
		'''
		@param int x :: x \in [1,3]
		@param int x :: x \in [1,3]
		'''
		if self.turn is 'Nothing':
			self.turn = elem
		if(self.turn is not elem):
			raise(APlayerCantPlayTwiceInARow(i18n.PLAYER_PLAYING_TWICE))
		if not (self.__inRange(x) and self.__inRange(y)):
			raise(PositionOutOfBounds(i18n.POSITIONS_OUT_OF_BOUNDS_MESSAGE))
		self.__putElem(x,y,elem)
		self.__nextTurn()

	def wins(self, player):
		'''
		@param string player :: player \in [<x> , <o>]
		'''
		hayGanador = False
		horizontales = [True, True, True]
		for i in xrange(0,3):
			horizontales[0] = horizontales[0] and self.array[i][0] is player
			horizontales[1] = horizontales[1] and self.array[i][1] is player
			horizontales[2] = horizontales[2] and self.array[i][2] is player
			if(self.array[i][0] is player and player is self.array[i][1] and player is self.array[i][2]):#verticales
				return True
		return horizontales[0] or horizontales[1] or horizontales[2] or self.__diagonalEqualsPlayer(player)



	def __diagonalEqualsPlayer(self,player):
		'''
		@param string player :: player \in [<x> , <o>]
		'''
		return self.array[1][1] is player and ((self.array[0][0] is player and self.array[2][2] is player) or ((self.array[0][2] is player and self.array[2][0] is player)))

	def __inRange(self,x):
		return 0 < x and x < 4

	def __putElem(self, x, y, elem):
		self.array[x-1][y-1] = elem

	def __nextTurn(self):
		if self.turn is i18n.X:
			self.turn = i18n.O
		else:
			self.turn = i18n.X

class TatetiTest(unittest.TestCase):


	def setUp(self):
		self.sut = Tateti()

	def test_Constructor(self):
		self.assertTrue(True)

	def test_emptyPosition(self):
		self.assertTrue(self.sut.isEmpty(2,3))

	def test_put_a_value_makes_the_position_no_longer_empty(self):
		self.sut.putO(1,1)
		self.assertFalse(self.sut.isEmpty(1,1))

	def test_o_wins_False(self):
		self.assertFalse(self.sut.wins(i18n.O))

	def test_o_wins_True(self):
		self.sut.putO(1,1)
		self.sut.putX(2,2)
		self.sut.putO(1,2)
		self.sut.putX(3,3)
		self.sut.putO(1,3)#exercise
		self.assertTrue(self.sut.wins(i18n.O))#assertion

	def test_put_a_value_in_a_negative_position(self):
		self.assertRaises(PositionOutOfBounds,self.sut.putO,0,0)

	def test_put_a_value_in_a_position_greater_than_3(self):
		self.assertRaises(PositionOutOfBounds,self.sut.putO,2,4)

	def test_player_x_plays_twice(self):
		self.sut.putX(1,1)
		self.assertRaises(APlayerCantPlayTwiceInARow,self.sut.putX, 2, 2)



if __name__ == '__main__':
	unittest.main()

